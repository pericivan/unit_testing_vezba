package unittesting.ssluzba.model;

import java.util.Date;
import java.util.Objects;

public class Enrollment {

	private Long id;
	private Date startDate;
	private Date endDate;

	private Course course;
	private Student student;

	
	public Enrollment(Long id, Date startDate, Date endDate, Course course, Student student) {
		super();
		this.id = id;
		this.startDate = startDate;
		this.endDate = endDate;
		this.course = course;
		this.student = student;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}
	
	@Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Enrollment e = (Enrollment) o;
        if(e.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, e.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

	@Override
	public String toString() {
		return "Enrollment [id=" + id + ", startDate=" + startDate
				+ ", endDate=" + endDate + "]";
	}
}
