package unittesting.ssluzba.model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Student {
	
	private Long id;
	private String cardNumber;
	private String firstName;
	private String lastName;

	private Set<Enrollment> enrollments = new HashSet<Enrollment>();
	private Set<Exam> exams = new HashSet<Exam>();
	
	

	public Student(Long id, String cardNumber, String firstName, String lastName, Set<Enrollment> enrollments,
			Set<Exam> exams) {
		super();
		this.id = id;
		this.cardNumber = cardNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.enrollments = enrollments;
		this.exams = exams;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Set<Enrollment> getEnrollments() {
		return enrollments;
	}

	public void setEnrollments(Set<Enrollment> enrollments) {
		this.enrollments = enrollments;
	}
	
	public Set<Exam> getExams() {
		return exams;
	}

	public void setExams(Set<Exam> exams) {
		this.exams = exams;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Student s = (Student) o;
		if (s.id == null || id == null) {
			return false;
		}
		return Objects.equals(id, s.id);
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(id);
	}

	@Override
	public String toString() {
		return "Student [id=" + id + ", cardNumber=" + cardNumber
				+ ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}
}
