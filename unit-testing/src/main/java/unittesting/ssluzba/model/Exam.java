package unittesting.ssluzba.model;

import java.util.Date;

public class Exam {

	private Long id;
    private Integer examPoints;
    private Integer labPoints;
    private Date date;
    
	private Course course;
	private Student student;
	private ExamPeriod examPeriod;
	
	public Exam(Long id, Integer examPoints, Integer labPoints, Date date, Course course, Student student,
			ExamPeriod examPeriod) {
		super();
		this.id = id;
		this.examPoints = examPoints;
		this.labPoints = labPoints;
		this.date = date;
		this.course = course;
		this.student = student;
		this.examPeriod = examPeriod;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getExamPoints() {
		return examPoints;
	}

	public void setExamPoints(Integer examPoints) {
		this.examPoints = examPoints;
	}

	public Integer getLabPoints() {
		return labPoints;
	}

	public void setLabPoints(Integer labPoints) {
		this.labPoints = labPoints;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	public Student getStudent() {
		return student;
	}

	public void setStudent(Student student) {
		this.student = student;
	}

	public ExamPeriod getExamPeriod() {
		return examPeriod;
	}

	public void setExamPeriod(ExamPeriod examPeriod) {
		this.examPeriod = examPeriod;
	}
}
