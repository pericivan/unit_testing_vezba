package unittesting.ssluzba.service;

import java.util.Collections;
import java.util.List;

import unittesting.ssluzba.model.Student;

public class StudentService {
	
	List<Student> studentRepository;
	
	public Student findOne(Long id) {
		return null;
	}

	public List<Student> findAll() {
		return Collections.emptyList();
	}

	public int save(Student student) {
		// returns ID of the saved student
		return 0;
	}

	
	public Student findByCard(String cardNumber) {
		return null;
	}
	
	public List<Student> findByLastName(String lastName) {
		return null;
	}
	
	public int calculateGrade(int points) {
		if (points < 0 || points > 100)
			throw new NumberFormatException();
		
		return points < 55 ? 5 : Math.round(points/(float) 10);
	}
}
