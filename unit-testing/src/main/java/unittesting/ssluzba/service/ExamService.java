package unittesting.ssluzba.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import unittesting.ssluzba.model.Exam;

public class ExamService {
	List<Exam> examRepository;
	
	public List<Exam> sortByExamPoints(List<Exam> listOfExams){
		Collections.sort(listOfExams, new Comparator<Exam>() {
		    public int compare(Exam exam1, Exam exam2) {
		    	return exam1.getExamPoints() - exam2.getExamPoints();
		    }
		});
		return listOfExams;
	}
	
	public boolean isExamPassed(Exam exam){
		return false;
	}
}
