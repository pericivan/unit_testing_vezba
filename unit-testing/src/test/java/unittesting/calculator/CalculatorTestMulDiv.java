package unittesting.calculator;

import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorTestMulDiv {
	
	@Test(enabled=false)
	public void testMul(){
		Calculator calculatorObject = new Calculator();
		// inicijalno postavim vrednost kalkulatora na 0
		calculatorObject.clear();
		// posto mnozenje sa nulom uvek daje rezultat 0, dodam recimo 3
		calculatorObject.add(3);
		// sada pomnozim broj u kalkulatoru sa 5 i testiram vrednost
		calculatorObject.multiply(5);
		int proizvod = calculatorObject.getResult();
		int ocekivaniProizvod = 3*5;
		assertEquals(ocekivaniProizvod, proizvod);
	}
	
	@Test
	public void testDiv(){
		Calculator calculatorObject = new Calculator();
		// inicijalno postavim vrednost kalkulatora na 0
		calculatorObject.clear();
		// dodam recimo 10 da bih broj 10 delio sa necim
		calculatorObject.add(10);
		// sada podelim broj u kalkulatoru sa 2 i testiram vrednost
		calculatorObject.divide(2);
		int kolicnik = calculatorObject.getResult();
		int ocekivaniKolicnik = 10/2;
		assertEquals(ocekivaniKolicnik, kolicnik);
	}
	
	@Test(expectedExceptions = java.lang.ArithmeticException.class)
	public void testDivByZero(){
		Calculator calculatorObject = new Calculator();
		// inicijalno postavim vrednost kalkulatora na 0
		calculatorObject.clear();
		// dodam recimo 10 da bih broj 10 delio sa necim
		calculatorObject.add(10);
		// sada podelim broj u kalkulatoru (10) sa 0 i treba da baci izuzetak
		calculatorObject.divide(0);
		// ako je bacen izuzetak test je prosao jer deljenje sa 0 zaista nije dozvoljeno
		// ako nije bacen izuzetak test je pao jer je izuzetak trebao da bude izazvan, a nije
	}
}
