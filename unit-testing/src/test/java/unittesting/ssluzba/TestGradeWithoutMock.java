package unittesting.ssluzba;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.junit.Assert.assertEquals;
import unittesting.ssluzba.service.StudentService;

public class TestGradeWithoutMock {

	@DataProvider(name = "gradePointsProvider")
	public static Object[][] provideData() {

		return new Object[][] { 
			{ 55, 6 }, 
			{ 68, 7 }, 
			{ 80, 8 },
			{ 92, 9 },
			{ 99, 10 }
		};
	}
	
	@Test(dataProvider = "gradePointsProvider")
	public void testBezMock(int points, int grade){
		StudentService studentService = new StudentService();
		int calculatedGrade = studentService.calculateGrade(points);
		assertEquals(grade, calculatedGrade);
	}
}
