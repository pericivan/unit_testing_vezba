package unittesting.ssluzba;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import unittesting.ssluzba.service.StudentService;
import static org.mockito.Mockito.when;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;

import org.mockito.Mockito;

public class TestGradeMock {
	
	StudentService studentService;
	
	@BeforeClass
	public void setUp(){
		studentService = Mockito.mock(StudentService.class);
		when(studentService.calculateGrade(anyInt())).thenCallRealMethod();
	}
	
	@Test(dataProvider = "gradePointsProvider", dataProviderClass = TestGradeWithoutMock.class)
	public void test(int points, int grade) {
		int calculatedGrade = studentService.calculateGrade(points);
		assertEquals(grade, calculatedGrade);
	}

}
