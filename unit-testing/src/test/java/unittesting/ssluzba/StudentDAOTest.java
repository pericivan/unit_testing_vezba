package unittesting.ssluzba;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import unittesting.book.Book;
import unittesting.book.BookDAO;
import unittesting.ssluzba.model.Enrollment;
import unittesting.ssluzba.model.Exam;
import unittesting.ssluzba.model.Student;
import unittesting.ssluzba.service.StudentService;

public class StudentDAOTest {

	  private static StudentService mockedStudentService;
	  private static Student student1;
	  private static Student student2;
	  private static Student student3;

	  @BeforeClass
	  public static void setUp(){		
		mockedStudentService = mock(StudentService.class);
		
	    Set enrollmentsStudent1 = new HashSet<Enrollment>();
	    Set enrollmentsStudent2 = new HashSet<Enrollment>();
	    Set enrollmentsStudent3 = new HashSet<Enrollment>();
	    
	    Set examsStudent1 = new HashSet<Exam>();
	    Set examsStudent2 = new HashSet<Exam>();
	    Set examsStudent3 = new HashSet<Exam>();
	    
		student1 = new Student(1L, "ra1-2014", "Ana", "Anic", enrollmentsStudent1 , examsStudent1);
		student2 = new Student(2L, "ra2-2014", "Marko", "Markovic", enrollmentsStudent2 , examsStudent2);
		student3 = new Student(3L, "ra3-2014", "Janko", "Jankovic", enrollmentsStudent3 , examsStudent3);

	    
		// mokujemo metodu koja treba da vrati listu svih studenata
	    when(mockedStudentService.findAll()).thenReturn(Arrays.asList(student1, student2, student3));
	    
	    // mokujemo metodu koja treba da vrati studenta za prosledjen ID. u ovom slucaju je mokovana za sve moguce vrednosti ID, iako NE MORA
	    when(mockedStudentService.findOne(1L)).thenReturn(student1);
	    when(mockedStudentService.findOne(2L)).thenReturn(student2);
	    when(mockedStudentService.findOne(3L)).thenReturn(student3);
	    
	    // mokujem za sve vrednosti, iako je dovoljno samo za jednu, posto cu test pisati samo za jednu od njih
	    when(mockedStudentService.save(student1)).thenReturn(1);
	    when(mockedStudentService.save(student2)).thenReturn(2);
	    when(mockedStudentService.save(student3)).thenReturn(3);
	    
	    when(mockedStudentService.findByCard("ra1-2014")).thenReturn(student1);
	    when(mockedStudentService.findByCard("ra2-2014")).thenReturn(student2);
	    when(mockedStudentService.findByCard("ra3-2014")).thenReturn(student3);
	    
	    when(mockedStudentService.findByLastName("Anic")).thenReturn(Arrays.asList(student1));
	    when(mockedStudentService.findByLastName("Markovic")).thenReturn(Arrays.asList(student2));
	    when(mockedStudentService.findByLastName("Jankovic")).thenReturn(Arrays.asList(student3));

	  }
	  
	  @Test
	  public void testFindAllStudents() {
	    List<Student> allStudents = mockedStudentService.findAll();
	    assertEquals(3, allStudents.size());
	  }
	  
	  @Test
	  public void testFindOne() {
	    Student foundStudent = mockedStudentService.findOne(1l);
	    
	    assertNotNull(foundStudent);
	    assertEquals(student1.getId(), foundStudent.getId());
	    assertEquals(student1.getCardNumber(), foundStudent.getCardNumber());
	    assertEquals(student1.getFirstName(), foundStudent.getFirstName());
	    assertEquals(student1.getLastName(), foundStudent.getLastName());
	    
	    // ili
	    
	    assertNotNull(foundStudent);
	    assertEquals(new Long(1), foundStudent.getId());
	    assertEquals("ra1-2014", foundStudent.getCardNumber());
	    assertEquals("Ana", foundStudent.getFirstName());
	    assertEquals("Anic", foundStudent.getLastName());
	    
	  }
	  
	  @Test
	  public void testSave() {
	    int savedStudentId = mockedStudentService.save(student2);
	    
	    assertEquals(2, savedStudentId);	    
	  }
	  
	  @Test
	  public void testFindByCard() {
	    Student foundStudent = mockedStudentService.findByCard("ra3-2014");
	   
	    assertNotNull(foundStudent);
	    assertEquals(new Long(3), foundStudent.getId());
	    assertEquals("ra3-2014", foundStudent.getCardNumber());
	    assertEquals("Janko", foundStudent.getFirstName());
	    assertEquals("Jankovic", foundStudent.getLastName());	    
	  }
	  
	  @Test
	  public void testFindByLastName() {
	    List<Student> foundStudents = mockedStudentService.findByLastName("Anic");
	    
	    assertEquals(1, foundStudents.size());
	    Student oneOfTheStudents = foundStudents.get(0);
	    assertEquals(new Long(1), oneOfTheStudents.getId());
	    assertEquals("ra1-2014", oneOfTheStudents.getCardNumber());
	    assertEquals("Ana", oneOfTheStudents.getFirstName());
	    assertEquals("Anic", oneOfTheStudents.getLastName());	
	  }
}
